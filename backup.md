# Ich heisse Sie herzlich Willkommen zu meinem neuen "Backup.md" !

### Erstellungsdatum: 19.09.2023

### @ Gianluca Anania, PE23b

---------------------------------

Inhaltsverzeichnis

[TOC]

# Aufträge:

- **Auftrag zu "Backup-Konzept für Ihr Notebook" vom 19.09.2023**

#### Auftragsbeschreibung: 

- Beschreiben Sie in Ihrem Lernjournal Ihr Backup-Konzept für Ihr Notebook und/oder Ihren Rechner zu Hause an Ihrem Schreibtisch. Das heisst, wie bekommen Sie Ihre Daten zurück, wenn Ihr Gerät kaputt gegangen ist und Sie ein neues benutzen müssen oder wenn Sie merken, dass Ihnen vor Monaten etwas verloren gegangen ist. Machen Sie dazu eine eigene Datei mit dem Namen "backup.md" in Ihrem Git-Repository (GitLab) und schreiben Sie alles was dazu gehört da hinein. Machen Sie von Ihrer ersten Lernjournal-Datei (README.md) einen Link auf diese neue Datei backup.md . Machen Sie vielleicht auch ein Bild in Ihr Text hinein. Achtung: Nur 1 Cloud-Lösung reicht nicht aus. Es geht um die "externe" Speicherung.


#### Auftragsbearbeitung:

![Bild: Ablagekonzept](https://gitlab.com/gianluca.anania/m231-doku/uploads/5801911cf0306c1eefdcb97c8d1aa1d6/Ablagekonzept.jpg)

![Bild: Backup_Konzept](https://gitlab.com/gianluca.anania/m231-doku/uploads/20a6348b5618b7357df6474108221e17/Backup_Konzept.jpg)

- Mit meinem persönlichen Backupkonzept bin ich gut abgesichert, wenn es zu Verlust der Daten kommt: 

- monatlich wird ein vollständiges "Time Machine" welches auf eine externe Festplatte gespeichert wird. 

- sobald neue Dateien dazukommen, werden diese auf iCloud gespeichert. Die Dateispeicherung geschieht sofort. 

- im Ausnahmefall wird also der neue MacBook mit der "Time Machine" wiederhergestellt und iCloud bringt die geänderten Daten auf's Gerät. 



- In Firmen ist die Server-Methode sinnvoll: Wenn es auf einem Gerät zum Datenverlust kommt, muss man sich keine Sorgen machen, da alles über die Server läuft. 

------------------------


# Ich heisse Sie herzlich Willkommen zu meinem Main Repository "m231-doku" !

## Lernjounrnal zum Modul 231

### Erstellungsdatum: 29.08.2023

### @ Gianluca Anania, PE23b

-----------------------------
# Inhaltsverzeichnis 

[TOC]

# Benutzerhandbuch

### Beschreibung Git Installation 

- 1. Google öffnen und "git download" suchen

- 2. Auf den ersten Link "https://git-scm.com/downloads" klicken und auf dem Desktop "Download for Windows" klicken 

- 3. Download for Windows x64 wählen und Installation ausführen

### Verknüpfung Gitlab Projekt mit PowerShell

- 1. Im eigenen Projekt (z.B. m231-doku) rechts oben auf "clone" drücken und "clone with https" wählen. 

- 2. Powershell öffnen und unter "workspace" den Befehl " git clone "clone-adresse" " ausführen.

### Git aktualisieren 

- 1. cd m231-doku 

- 2. git add. 

- 3. git commit -m "Update X.X.X - DD.MM.JJJJ"

- 4. git push 

##### Status von Git prüfen 

- git status 

##### Wenn auf einen anderen Gerät oder online gearbeitet

- git pull (für aktualisieren von Gitlab, wenn auf einen anderen Gerät bearbeitet wurde)

#### Wenn Git nicht funktioniert

- git config --global user.email

- git config --global user.name


### CMD einstellen 

- 1. c:

- 2. cd users

- 3. cd NUTZERNAME

- 4. cd workspace

- 5. cd m231-doku

-> hier: auch den Befehl "dir" für eine Übersicht aller Möglichkeiten

-----------------------------

-----------------------------

## Lernziele LB1 (Der Gebrauch des Lernjournals und des Internets ist erlaubt)

- Datenschutz und Datensicherheit, Datenschutzgesetz DSG (Grundsätze)

- Git-Konzept (grobe Funktionsweise lokal und in der Cloud), Git-(Grund-)Befehle

- Datensicherheit

-----------------------------

-----------------------------

# Aufträge 


#### Auftrag zu "Backup-Konzept für Ihr Notebook" vom 19.09.2023

- Auftragsbearbeitung unter: backup.md Link: [Link zu backup.md](https://gitlab.com/gianluca.anania/m231-doku/-/blob/main/backup.md)

#### Auftrag zu "Notwendigkeit von Passwörter - Was sind sichere Passwörter?"

<img src="IMG_5556.jpg" width=600 />

#### Auftrag zu "M231-6a Authentisierung-Authentifizierung-Autorisierung"

### Begrifferklärung

- Authentisierung bezeichnet das Nachweisen einer Identität. 

- Authentifizierung bezeichnet die Prüfung dieses Identitätsnachweises auf seine Authentizität. 

- Autorisierung bezeichnet das Gewähren des Zugangs zu den Privilegien, welche der erfolgreich nachgewiesenen Identität zustehen.

#### Auftrag zu "M231-6b Multifaktor Authentisierung"

### Begrifferklärung

- die 2 Faktor-Authentisierung:  Die Zwei-Faktor-Authentifizierung ist eine Sicherheitsprozedur, bei der ein Anwender zwei unterschiedliche Merkmale bereitstellt, um sich zu identifizieren.  

- Beispiel: E-Banking: Login mit Passwort -> zusätzlich ein SMS Code

- die 3 Faktor-Authentisierung: Gerade im professionellen Umfeld kommen auch Verfahren mit drei und mehr Faktoren zum Einsatz. Erfordert die Anmeldung mindestens ein Merkmal aus jeder der drei Gruppen, spricht man von Drei-Faktor-Authentifizierung (3FA). Ein Beispiel wäre die Kombination aus Passwort, Geräte-ID und Fingerabdruck.

- Beispiel-Foto: <img src="3FA_Beispiel.jpg" width=600 />


#### Auftrag zu "M231-6c Passwort-Manager einrichten"

- Laptop Screenshot: <img src="PWM.Laptop.jpg" width=600 />

- Handy Screenshot: <img src="PWM.Handy.jpg" width=600 />

#### Auftrag zu "M231-8a Problematik von Datenlöschungen"

- Die Problematik von Datenlöschungen über alle Archive und Backups liegt in der endgültigen Unwiederbringlichkeit der Informationen. 
Wenn sämtliche Sicherungskopien gelöscht werden, gehen potenziell entscheidende Daten für immer verloren. 
Dies kann schwerwiegende Konsequenzen für Unternehmen oder Organisationen haben, da wichtige Informationen, 
Historien oder Sicherungsmöglichkeiten komplett eliminiert werden, was zu schweren Datenverlusten und operativen Problemen führen kann. 
Daher erfordert die Löschung von Daten über alle Archive und Backups eine äußerst sorgfältige Abwägung der Konsequenzen und eine umfassende 
Planung, um irreparable Schäden zu vermeiden.

#### Auftrag zu " M231-8b Impressum, Disclaimer, AGB"

###### Auftrag 1: Übersetzung Impressum

- Deutsch (Synonym): "Anbieterkennzeichnung"
Englisch: "Imprint"
Französisch: "Mentions légales"
Italienisch: "Impressum" (Es wird oft unverändert verwendet.)
Spanisch: "Aviso legal"

###### Auftrag 2: Bedeutung "Impressum"

- Ein "Impressum" ist eine rechtliche Angabe auf Websites oder in anderen Medien, 
die Informationen über den Verantwortlichen bereitstellt. Es enthält wichtige Angaben wie Name, 
Kontaktdaten und gesetzlich vorgeschriebene Informationen, um Transparenz zu schaffen und Nutzern 
die Identifikation des Verantwortlichen zu ermöglichen.

###### Auftrag 3: Impressum-Pflicht in der CH

- In der Schweiz besteht Impressum-Pflicht für geschäftsmäßig betriebene Websites sowie private Websites 
mit geschäftsmäßigen Elementen gemäß der Verordnung über die Benützung von Fernmeldeanlagen. 
Private Websites ohne geschäftsmäßige Ausrichtung können von der Impressumspflicht befreit sein. 

###### Auftrag 4: Impressum-Pflich in der EU

- In Deutschland und der EU besteht Impressumspflicht für geschäftsmäßige Internetangebote,
 sowohl private als auch kommerzielle. Erforderliche Angaben umfassen Name, Adresse, E-Mail und Telefonnummer des Anbieters,
 sowie gegebenenfalls Handelsregistereintrag und Umsatzsteuer-Identifikationsnummer. Die Schweiz hat ähnliche Prinzipien, 
 aber spezifische Anforderungen können unterschiedlich sein. 
 
- Unterschiede: Es gibt Unterschiede zwischen der EU und der Schweiz bezüglich der Impressum-Pflicht. 
Die EU regelt dies durch die Richtlinie 2000/31/EG, während die Schweiz ihre Impressumspflicht 
durch die Verordnung des Eidgenössischen Departements für Umwelt, Verkehr, Energie und Kommunikation festlegt. 
Die grundlegenden Anforderungen sind ähnlich, aber spezifische Details können variieren. 

###### Auftrag 5: Übersetzung "Disclaimer"

- Deutsch: "Haftungsausschluss"
Englisch (Synonym): "Exclusion of Liability"
Französisch: "Clause de non-responsabilité"
Italienisch: "Esclusione di responsabilità"
Spanisch: "Descargo de responsabilidad"

###### Auftrag 6: Bedeutung "Disclaimer"

- Ein "Disclaimer" ist eine rechtliche Erklärung auf einer Website, die Haftungsausschlüsse, 
rechtliche Hinweise oder Beschränkungen kommuniziert. Dies dient dazu, die Haftung des Website-Betreibers zu begrenzen, 
urheberrechtliche Aspekte zu klären und den Nutzern bestimmte Informationen zu geben, um rechtliche Konflikte zu vermeiden.

###### Auftrag 7: Bedeutung "AGB"

- Die Abkürzung "AGB" steht für "Allgemeine Geschäftsbedingungen". AGB sind rechtliche Dokumente, die die Bedingungen und Regelungen festlegen, 
unter denen ein Unternehmen seine Produkte oder Dienstleistungen anbietet. In den AGB finden sich typischerweise Informationen zu 
Zahlungsmodalitäten, Lieferbedingungen, Gewährleistung, Haftungsausschlüssen, Datenschutzbestimmungen und anderen wichtigen Aspekten 
des Vertragsverhältnisses zwischen dem Unternehmen und dem Kunden. Diese Bedingungen dienen dazu, die Rechte und Pflichten 
beider Parteien klar zu definieren und potenzielle Konflikte zu minimieren, indem sie die Grundlage für die Geschäftsbeziehung schaffen. 
Es ist üblich, dass Kunden vor Abschluss eines Geschäfts diese AGB lesen und akzeptieren müssen.

###### Auftrag 8: AGB-Beispiele 

- AGB Generator: https://www.shopify.com/de/tools/richtlinien-generator/richtlinie-fur-geschaftsbedingungen
- AGB von Digitec Galaxus AG: https://www.galaxus.ch/de/wiki/478
- AGB Vorlagen: https://vorlage-kostenlos.de/agb/
- AGB Dienstleistungen der Swisscom: https://www.swisscom.ch/content/dam/swisscom/nl/rechtliches/res/agb-dienstleistung_de.pdf
- AGB der schweizerischen Post: https://www.post.ch/de/pages/footer/allgemeine-geschaeftsbedingungen-agb

#### Auftrag zu " M231-9 Lizenzmodelle"

###### Auftrag 1: Erklären Sie das Wort "Lizenz".

- Eine "Lizenz" ist eine formelle Erlaubnis oder Genehmigung, die von einer autorisierten Stelle erteilt wird. 
Im rechtlichen Kontext bezieht sie sich auf die Erlaubnis, bestimmte Handlungen auszuführen, 
wie das Betreiben eines Geschäfts oder das Führen eines Fahrzeugs. 
Im Bereich der Software bedeutet eine Lizenz das Recht zur Nutzung, Vervielfältigung oder Modifikation einer Software 
gemäß den Bedingungen, die vom Softwarehersteller festgelegt wurden.

###### Auftrag 2: Warum kommt man auf die Idee, Lizenzen zu schaffen?

- Kontrolle und Schutz: Lizenzen ermöglichen es den Inhabern, die Verwendung ihrer geistigen Eigentumsrechte, wie etwa Software, Marken oder Patente, zu kontrollieren und zu schützen. Dies schützt vor unautorisierten Nutzung oder Verbreitung.

- Einnahmen generieren: Die Vergabe von Lizenzen kann eine Einnahmequelle für Rechteinhaber darstellen. Durch die Erlaubnis gegen Gebühr können sie Einnahmen aus der Nutzung ihrer Produkte oder Dienstleistungen generieren.

- Regulierung: Im Fall von staatlichen Lizenzen, wie Führerscheinen oder Gewerbelizenzen, dient dies der Regulierung und Kontrolle bestimmter Aktivitäten, um die öffentliche Sicherheit, Gesundheit oder Ordnung zu gewährleisten.

- Förderung von Innovation: Im Bereich von Open-Source-Softwarelizenzen fördern diese die Zusammenarbeit und den Austausch von Ideen, indem sie es Entwicklern erlauben, den Quellcode frei zu teilen und zu modifizieren, unter der Bedingung, dass ihre Änderungen ebenfalls offen geteilt werden.

###### Auftrag 3: Worauf gibt es alles Lizenzen ausser für Software?

- Musik und Unterhaltung: Künstler und Plattenfirmen erteilen Lizenzen für die Verwendung und Verbreitung von Musik, sei es in Form von CDs, digitalen Downloads, Streaming oder öffentlichen Aufführungen.

- Bildung: Lehrmaterialien, Bücher und bestimmte Formen von intellektuellem Eigentum können für Bildungszwecke lizenziert werden, um den Zugang zu bestimmten Ressourcen zu regeln.

- Marken: Unternehmen erteilen Lizenzen für die Verwendung ihrer Marken und Logos, um sicherzustellen, dass diese nur gemäß den spezifischen Richtlinien und Standards verwendet werden.

- Film und Fernsehen: Filmproduktionsunternehmen lizenzieren ihre Filme und Fernsehsendungen für die Ausstrahlung, Streaming, DVD-Veröffentlichung und andere Vertriebskanäle.

- Sportartikel und Merchandising: Sportteams und Marken erteilen Lizenzen für die Herstellung und den Verkauf von Produkten wie Bekleidung, Ausrüstung und Merchandising-Artikeln mit ihren Logos.

- Patente: Unternehmen erteilen Lizenzen für die Nutzung ihrer patentierten Technologien oder Erfindungen, entweder gegen Gebühr oder im Rahmen von Kooperationsvereinbarungen.

- Gewerbelizenzen: Diese umfassen Genehmigungen für den Betrieb von Unternehmen in bestimmten Branchen, wie beispielsweise Alkoholverkaufslizenzen oder Baugenehmigungen.

###### Auftrag 4: Aufzählung Lizenzmodelle

-Proprietäre Lizenz: Der Inhaber behält die Kontrolle über die Software oder das geistige Eigentum und gewährt Nutzungsrechte unter bestimmten Bedingungen. Der Quellcode ist normalerweise nicht offen.

- Open-Source-Lizenz: Erlaubt freien Zugang zum Quellcode der Software. Es gibt verschiedene Arten, darunter die GNU General Public License (GPL) und die MIT-Lizenz.

- Freeware-Lizenz: Kostenlose Software, für die keine Gebühr erhoben wird, aber der Quellcode ist möglicherweise nicht zugänglich oder modifizierbar.

- Kostenpflichtige Lizenz: Benutzer zahlen eine Gebühr für die Nutzung von Software oder Dienstleistungen. Kann als Einmalzahlung oder Abonnementmodell vorliegen.

- Creative Commons-Lizenz: Wird oft für kreatives Material wie Bilder, Texte oder Musik verwendet. Es gibt verschiedene Varianten, die die Bedingungen für die Nutzung festlegen.

- Enterprise-Lizenz: Speziell für Geschäftsumgebungen konzipiert. Bietet oft erweiterte Funktionen und Support.

- GPL (General Public License): Eine Open-Source-Lizenz, die die Freiheit der Softwarenutzer betont und sicherstellt, dass abgeleitete Werke ebenfalls Open Source bleiben.

- SaaS-Lizenz (Software as a Service): Benutzer zahlen für den Zugang zu Softwareanwendungen über das Internet, normalerweise auf monatlicher oder jährlicher Basis.

###### Auftrag 5: Was bedeutet "open source" und was kann man mit solcher Software machen?

- "Open Source" bezieht sich auf eine Art von Software, deren Quellcode öffentlich zugänglich ist und von der Gemeinschaft eingesehen, genutzt, modifiziert und weitergegeben werden kann. Im Wesentlichen bedeutet dies, dass die inneren Arbeitsweisen der Software transparent und für jeden einsehbar sind.
Mit Open-Source-Software kann man folgendes machen:

- Nutzung: Benutzer können die Software frei herunterladen, installieren und verwenden, oft ohne Kosten.

- Modifikation: Der Quellcode kann von Entwicklern geändert oder angepasst werden, um die Software an spezifische Bedürfnisse anzupassen oder Fehler zu beheben.

- Weitergabe: Modifizierte Versionen können weitergegeben werden, entweder in der ursprünglichen oder modifizierten Form.

- Kollaboration: Die Entwickler-Community kann gemeinsam an der Verbesserung der Software arbeiten. Fehler können gemeldet und behoben werden, und neue Funktionen können hinzugefügt werden.

###### Auftrag 6: Was ist der Unterschied zwischen "copyright" und "copyleft"?

- Copyright ist ein rechtlicher Schutz, der dem Schöpfer oder Urheber eines Originalwerks automatisch gewährt wird. Es gibt dem Urheber das ausschließliche Recht, das Werk zu reproduzieren, zu verteilen, öffentlich aufzuführen und abgeleitete Werke zu erstellen.
In der Regel wird mit Copyright eine restriktive Kontrolle über das Werk ausgeübt, und andere dürfen es ohne Zustimmung des Urhebers nicht reproduzieren oder verwenden.
Copyleft:

- Copyleft ist eine spezielle Art von Lizenz, die auf Open-Source-Software angewendet wird. Anders als Copyright, das restriktive Kontrolle ausübt, fördert Copyleft die Freiheit der Software.
Copyleft-Lizenzen, wie die GNU General Public License (GPL), erlauben es anderen, den Quellcode einer Software zu sehen, zu ändern und zu verteilen. Allerdings müssen die abgeleiteten Werke unter denselben Bedingungen wie die ursprüngliche Software veröffentlicht werden, was sicherstellt, dass die Freiheiten und Rechte erhalten bleiben.

- In Kurzform: Copyright schützt die exklusiven Rechte des Urhebers, während Copyleft die Freiheit fördert, indem es anderen erlaubt, den Code zu nutzen und zu modifizieren, solange sie die gleichen Freiheiten für ihre Änderungen gewährleisten.

###### Auftrag 7:  Welches Lizenz-Modell wird angewendet, wenn man...

- im App-Store eine zu bezahlende App hereunterlädt und installiert?

Wenn Sie im App-Store eine zu bezahlende App herunterladen und installieren, handelt es sich in der Regel um ein kostenpflichtiges Lizenzmodell. 
Dies bedeutet, dass Sie für das Recht bezahlen, die App zu nutzen, jedoch nicht notwendigerweise den Quellcode oder das Recht haben, die Anwendung zu modifizieren oder zu verbreiten. 
Die genauen Bedingungen können je nach den Nutzungsbedingungen der App und den Lizenzbestimmungen des Entwicklers variieren. Es ist wichtig, die Lizenzvereinbarungen zu lesen, 
um zu verstehen, welche Rechte und Einschränkungen mit dem Kauf der App verbunden sind.

- im App-Store eine Gratis-App herunterlädt und installiert?

Wenn Sie im App-Store eine Gratis-App herunterladen und installieren, bedeutet das nicht zwangsläufig, dass die Software ohne Lizenz ist. Kostenlose Apps unterliegen normalerweise den Bedingungen einer Lizenz, die durch den App-Store-Anbieter und/oder den Entwickler festgelegt wird. Oftmals handelt es sich um proprietäre Lizenzen, die die Nutzung und Verteilung der App regeln.

Diese Lizenzen können verschiedenen Bedingungen umfassen, beispielsweise:

1. **Nutzungsbedingungen:** Sie legen fest, wie Sie die App verwenden können, z. B. für persönliche oder kommerzielle Zwecke.

2. **Urheberrechtsbestimmungen:** Sie geben an, wer das geistige Eigentum an der App besitzt, und regeln die Reproduktion und Verbreitung der App.

3. **Einschränkungen:** Es können Beschränkungen hinsichtlich der Modifikation der App oder der Erstellung abgeleiteter Werke festgelegt sein.

- Haben Sie die Software bezahlt, als Sie ihr aktuelles Smartphone gekauft/bekommen haben? Wenn Sie es nicht wissen, ob Sie extra bezahlt haben, wie und wann bezahlen Sie?

Die Kosten für die Software, wie das iOS-Betriebssystem bei iPhones, sind in den Gesamtkosten des Geräts enthalten. Die Updates für das Betriebssystem werden in der Regel kostenlos zur Verfügung gestellt.

--------------------------------

--------------------------------

# Reflexion von Gianluca Anania zum Modul 231 

### 07.11.2023 - die Letzte

#### Das möchte ich heute machen: 

- Lernjournal verschönern, anpassen

#### Das habe ich heute gemacht: 

- Lernjournal vollständig bererit zur Abgabe/Bewertung. 

#### Das habe ich heute gelernt: 

- Gelernt, wie man ein automatisches Powershell-Backup erstellt. 

### Das ist mir persönlich wichtig:

- Das die Bewertung des Lernjournal gut verläuft. 

- Gute Note erreichen. 

#### Das ist mir gut gelungen: 

- Alle Aufträge erledigt.

#### So bin ich vorgegangen, damit es mir gelingt:

- Mit Kollgenen abgesprochen und gegenseitig geholfen. 

#### Damit hatte ich Schwierigkeiten:

- Viele Nachrichten im Geschäftlichen Bereich erhalten, was zu Ablenkung geführt hat.

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich mit einem guten Gewissen das Lernjounal abgeben kann!

### End of the Day. - Gianluca Anania

-------------------------------

### 31.10.2023

#### Das möchte ich heute machen: 

- LB2 besprechen

- Aufträge beenden

- Lernjounal verschönern 

#### Das habe ich heute gemacht: 

- LB 2 zurückerhalten. Bin sehr zufreiden. 

- Auftrag 9 - Lizensmodelle beendet. 

- Zusatzauftrag "automa. Backup mit Powershell" erledigt. 

#### Das habe ich heute gelernt: 

- Gelernt, wie man ein automatisches Powershell-Backup erstellt. 

### Das ist mir persönlich wichtig:

- Weiterhin eine gute Balance zwischen Einzelarbeit, Partnerarbeit und Input. 

#### Das ist mir gut gelungen: 

- Alle Aufträge erledigt 

- Zusatzauftrag: Automatisches Backup mit Powershell erledigt. 

#### So bin ich vorgegangen, damit es mir gelingt:

- Mit Sitznachbaren abgesprochen und gegenseitig geholfen. 

#### Damit hatte ich Schwierigkeiten:

- heute hab ich mich einfach ablenken lassen, weil es zwischendurch "freie" Timeslots gab.

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich mit einem guten Gewissen das Lernjounal abgeben kann!

### End of the Day. - Gianluca Anania

-------------------------------

### 24.10.2023

#### Das möchte ich heute machen: 

- Bei der LB2 gut abschliessen. 

- Alle offenen Aufträge beenden. 

#### Das habe ich heute gemacht: 

- LB 2 abgeschlossen, Resultat folgt. 

- Auftrage, die auf heute fällig waren/sind, beendet. 

#### Das habe ich heute gelernt: 

- Gelernt, was die Porblematiken bei Datenlöschungen sind. 

- Gelernt, was Impressum, Disclaimer, AGB sind und bedeuten. 

### Das ist mir persönlich wichtig:

- Weiterhin eine gute Balance zwischen Einzelarbeit, Partnerarbeit und Input. 

#### Das ist mir gut gelungen: 

- Ich konnte alle Aufträge vollenden. 

#### So bin ich vorgegangen, damit es mir gelingt:

- Tipps durch Mitlernenden eingeholt. 

#### Damit hatte ich Schwierigkeiten:

- Anfangs ging Gitlab mal wieder nicht. -> neu clonen

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich alles beendet habe. 

### End of the Day. - Gianluca Anania

-------------------------------

### 03.10.2023

#### Das möchte ich heute machen: 

- Den Auftrag "Datenverschlüsselung - Cäsar, Vigenère" erledigen

#### Das habe ich heute gemacht: 

- Gruppen-Challange zu "Gute Verschlüsselungs-Methoden"

- Auftrag beendet. 

- Videos dazu angeschaut und studiert. 

#### Das habe ich heute gelernt: 

- Gelent, wie die Verschüsselungs-Methoden funktionieren und auch angewendet.

### Das ist mir persönlich wichtig:

- Weiterhin eine gute Balance zwischen Einzelarbeit, Partnerarbeit und Input. 

#### Das ist mir gut gelungen: 

- Ich konnte alle Aufträge vollenden. 

#### So bin ich vorgegangen, damit es mir gelingt:

- Tipps durch Mitlernenden eingeholt und ausgetauscht.

#### Damit hatte ich Schwierigkeiten:

- Konzentration, da bald Ferien sind und Unsicherheit der LB vom anderen Modul. 

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich alles beendet habe. 

### End of the Day. - Gianluca Anania

-------------------------------

#

# Ferien

#

-------------------------------

### 26.09.2023

#### Das möchte ich heute machen: 

- Alle offenen Aufträge beenden. 

#### Das habe ich heute gemacht: 

- LB 1 sehr gut abgeschlossen!!!!

- Auftrage, die auf heute fällig waren/sind, beendet. 

#### Das habe ich heute gelernt: 

- Gelernt, was die verschiedenen MFA sind. 

### Das ist mir persönlich wichtig:

- Weiterhin eine gute Balance zwischen Einzelarbeit, Partnerarbeit und Input. 

#### Das ist mir gut gelungen: 

- Ich konnte alle Aufträge vollenden. 

#### So bin ich vorgegangen, damit es mir gelingt:

- Tipps durch Mitlernenden eingeholt. 

#### Damit hatte ich Schwierigkeiten:

- Konzentration, da ein wenig erschöpft und Kopfschmerzen. 

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich alles beendet habe. 

### End of the Day. - Gianluca Anania

-------------------------------

### 19.09.2023

#### Das möchte ich heute machen: 

- LB 1 abschliessen 

- Alle offenen Aufträge beenden. 

#### Das habe ich heute gemacht: 

- LB 1 geschrieben 

- Auftrage, die auf heute fällig waren/sind, beendet. 

#### Das habe ich heute gelernt: 

- Gelernt, wie man eine optimale Datensicherung hat. 

### Das ist mir persönlich wichtig:

- Weiterhin eine gute Balance zwischen Einzelarbeit, Partnerarbeit und Input. 

#### Das ist mir gut gelungen: 

- Ich konnte alle Aufträge vollenden. 

#### So bin ich vorgegangen, damit es mir gelingt:

- Tipps durch Mitlernenden eingeholt. 

#### Damit hatte ich Schwierigkeiten:

- Konzentration, da ein wenig erschöpft und Kopfschmerzen. 

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich alles beendet habe. 

### End of the Day. - Gianluca Anania

-------------------------------

### 12.09.2023

#### Das möchte ich heute machen:

- Heute möchte ich  das Lernjournal an Hr. Mueller freigeben. 

- Ausserdem möchte ich das Datenschutzgesetz nochmals anschauen.

- Zudem schauen wir uns in der Klasse das Thema "Backup" an. 

- Alle Aufträge für heute beenden. 

- Mich gut auf die LB1, die nächste Woche stattfindet, vorbereiten. 

#### Das habe ich heute gemacht: 

- Lernjournal habe ich freigegeben.

- Datenschutzgesetz überflogen

- Backup Thema grösstenteils verstanden 

- Die Teams-Aufträge werden aufgrund von Zeitmangels verschoben. 

- LB1 Vorbereitung folgt noch. 

#### Das habe ich heute gelernt: 

- Heute habe ich vieles über Backup erlernt, weshalb es wichtig ist und wie das funktioniert. z.B. Image-Backup -> 1-1 Kopie des Gerätes

- Inkrementelle und Diffrentielle Backups haben einen grossen Unterschied. 

- -> Inkrementelle: Die Dateien, welche geändert wurden, werden erneut gesichert und überschrieben. --> Zeiteffizienter, sofort

- -> Diffrentielle: Das gesamte Paket wird erneut gesichert, somit erneutes vollständiges Backup. --> Sicherer/Genauer, muss nachts, wenn niemand arbeitet, gesichert werden. 

### Das ist mir persönlich wichtig:

- Weiterhin eine gute Balance zwischen Einzelarbeit, Partnerarbeit und Input. 

#### Das ist mir gut gelungen: 

- Ich konnte einige Inputs in der Partnerarbeit miteinbeziehen. 

#### So bin ich vorgegangen, damit es mir gelingt:

- Den Leseauftrag gut durchlesen und wichtiges anstreichen. 

#### Damit hatte ich Schwierigkeiten:

- Konzentration, da ein wenig erschöpft und Kopfschmerzen. 

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich mit guten Gewissen in den Mittag gehen kann und keine Bedenken aufgrund einer Prüfung habe. 

### End of the Day. - Gianluca Anania

-----------------------------

### 05.09.2023

#### Das möchte ich heute machen: 

- Heute möchte ich mein Ablagekonzept fertig gestallten und abgeben. 

#### Das habe ich heute gemacht: 

- Das Diagramm ist beendet und abgegeben. 

- An Gitlab sind wir auch fleissig am Üben. 

- Neu haben wir Notepad++ heruntergeladen um unser Lernjournal zu erfassen.

- Ausserdem haben wir heute viel über Datensicherheit und Datenschutz gesprochen. Zudem haben wir ein Arbeitsblatt erhalten und in der Klasse besprochen. 

#### Das habe ich heute gelernt: 

- Wir haben viel über Datensicherheit und Datenschutz gelernt. z.B. Wo man Kamera aufbauen darf und was genau beachtet werden muss. 

#### Das ist mir persönlich wichtig:

- Die Arbeitshaltung soll so bleiben. 

#### Das ist mir gut gelungen: 

- Heute gute Konzentration. 

#### So bin ich vorgegangen, damit es mir gelingt:

- Gut ausschlafen und nicht ablenken lassen. 

#### Damit hatte ich Schwierigkeiten:

- keine... 

#### Ich bin zufrieden mit meiner Leistung, weil...

- Ich meine Ziele erreicht habe. 



### End of the Day. - Gianluca Anania

-----------------------------

### 29.08.2023

#### Das möchte ich heute machen: 

- Heute möchte ich meine angefangene Tabelle beenden und mit den nächsten Schritt "Diagramm_Ablagekonzept" beginnen. 

#### Das habe ich heute gemacht: 

- Die Tabelle ist beendet und abgegeben. Das Diagramm ist in Bearbeitung. Gitlab sind wir fleissig am Üben. Das Lernjournal wird im Editor geschrieben.

#### Das habe ich heute gelernt:

- Ich habe gelernt, wie man eine Git-Verknüpfung auf dem Laptop bringt. Ausserdem habe ich viele neue cmd gelernt und angewendet. 

#### Das ist für mich persönlich wichtig:

- Das die Arbeitshaltung so bleibt. 

#### Das ist mir gut gelungen:

- Ich konnte vieles Neues lernen. Ich konnte anderen Mitschülern auch mit dem Git helfen. 

#### So bin ich vorgegangen, damit es mir gelingt.

- gut zuhören und motiviert sein

#### Damit hatte ich Schwierigkeiten:

-  keine... 

#### So habe ich auf die Schwierigkeiten reagiert:

- ...

#### So habe ich mich heute gefühlt:

- motiviert 

#### Ich bin zufrieden mit meiner Leistung:

- sehr weit gekommen und ich konnte den anderen Mitschülern helfen. 


### End of the Day - Gianluca Anania
